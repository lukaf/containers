package main

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
)

type container struct {
	path    string
	name    string
	command string
}

func (c container) Run() error {
	fmt.Printf("Running %s", c.command)

	command := strings.Split(c.command, " ")

	cmd := exec.Command(command[0], command[1:]...)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	cmd.Stdin = os.Stdin

	return cmd.Run()
}

func NewContainer(name string, config *configuration) *container {
	container := &container{
		name: name,
		path: filepath.Join(config.base, name),
	}

	container.command = fmt.Sprintf(config.command, container.path)

	return container
}
