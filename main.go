package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

func fatal(msg ...interface{}) {
	fmt.Println(msg...)
	os.Exit(1)
}

func main() {
	var (
		containerName  string
		listContainers bool
	)

	flag.StringVar(&containerName, "c", "", "Container name")
	flag.BoolVar(&listContainers, "l", false, "List containers")
	flag.Parse()

	cfg, err := NewConfiguration("", "") // use defaults
	if err != nil {
		fatal(err)
	}

	if listContainers {
		for _, c := range list(cfg.base) {
			fmt.Println(c)
		}
		return // os.Exit??
	}

	if containerName == "" {
		fatal("missing container name")
	}

	c := NewContainer(containerName, cfg)

	if err := c.Run(); err != nil {
		fatal(err)
	}
}

func list(path string) []string {
	out := make([]string, 0)

	files, err := ioutil.ReadDir(path)
	if err != nil {
		fmt.Println(err)
		return out
	}

	for _, file := range files {
		stat, err := os.Stat(filepath.Join(path, file.Name()))
		if err != nil {
			fmt.Println(err)
			continue
		}

		if stat.IsDir() {
			out = append(out, file.Name())
		}
	}

	return out
}
