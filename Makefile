TARGET := containers
SOURCES := *.go

$(TARGET): $(SOURCES)
	go vet .
	go build -o $(TARGET)

install: $(TARGET)
	chmod +x $(TARGET)
	mv $(TARGET) ~/bin/.
