package main

import (
	"errors"
	"os"
	"path/filepath"
)

type configuration struct {
	base    string
	command string
}

var defaults = struct {
	base    string
	command string
}{
	"~/containers/nspawn",
	"sudo systemd-nspawn -D %s --bind-ro=/tmp/.X11-unix --bind=/dev/snd --bind=/run/user/1000/pulse:/run/user/host/pulse -b",
}

func NewConfiguration(base string, command string) (*configuration, error) {
	c := &configuration{
		base:    base,
		command: command,
	}

	if c.base == "" {
		c.base = defaults.base
	}

	if c.command == "" {
		c.command = defaults.command
	}

	fullPath, err := expandPath(c.base)
	if err != nil {
		return nil, err
	}

	if c.base != fullPath {
		c.base = fullPath
	}

	stat, err := os.Stat(c.base)
	if err != nil {
		return nil, err
	}

	if !stat.IsDir() {
		return nil, errors.New("path is not a directory")
	}

	return c, nil
}

func expandPath(path string) (string, error) {
	if path == "" {
		return "", errors.New("empty path")
	}

	if path[0] == '~' {
		return filepath.Join(os.Getenv("HOME"), path[1:]), nil
	}

	if path[0] != '/' {
		return "", errors.New("path must be absolute")
	}

	return path, nil
}
